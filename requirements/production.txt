django
# Templates
django_compressor

# Security
bleach

# Celery: Message queue
#celery
#django-celery

# Caching
python-memcached

# Admin
django-debug-toolbar

# Registration /Authentication
django-allauth

# Image processing
Pillow

mysqlclient
sqlparse


billiard
django-appconf
django-extensions
bcrypt

alabaster
asn1crypto
Babel
coverage
cryptography
docutils
fabric
idna
imagesize
invoke
isort
Jinja2
lazy-object-proxy
MarkupSafe
mccabe
nose
oauthlib
packaging
paramiko
pep8
pycparser
pyflakes
Pygments
PyNaCl
pyparsing
rjsmin
snowballstemmer
typed-ast
wrapt


django-filter
django-imagekit
django-cleanup

django-tables2
tablib