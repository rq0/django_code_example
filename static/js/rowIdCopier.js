document.addEventListener('DOMContentLoaded', clickListener);
document.addEventListener('DOMContentLoaded', domModifiedObserve);

function clickListener() {
    document.querySelectorAll("tr").forEach(row => row.addEventListener("click", (clickEvent) => {
        var isRWillBeSaved = !clickEvent.altKey && row.id[0] === 'r' && location.hostname === "ruobr.ru"; //Чтоб удалить не удалять первую r из id в ЭШ
        if (clickEvent.ctrlKey || clickEvent.metaKey) {
            if (clickEvent.shiftKey) {
                let rowIds = "";
                const rows = row.parentNode.querySelectorAll("tr");
                rows.forEach(r => {
                    if (r.id) {
                        if (!rowIds.length)
                            rowIds = addOneId(r.id);
                        else rowIds = `${rowIds}, ${addOneId(r.id)}`;
                    }
                });
                writeBuffer(rowIds);
            } else {
                writeBuffer(addOneId(row.id))
            }
        }

        function addOneId(idValue) {
            if (isRWillBeSaved)
                return idValue.slice(1);
            else return idValue;

        }
    }));

    function writeBuffer(value) {
        if (navigator.clipboard)
            navigator.clipboard.writeText(value).then(() => {
                Swal.fire({
                    title: 'Скопировано',
                    timer: 1200,
                    timerProgressBar: true,
                    text: value,
                    position: 'bottom-start',
                    showConfirmButton: false
                })
            });
        else {
            Swal.fire({
                title: 'Буфер обмена недоступен',
                timer: 1200,
                timerProgressBar: true,
                text: value,
                position: 'bottom-start',
                showConfirmButton: false
            });
            console.warn('Попытка скопировать значение:', value)
        }
    }
}


function domModifiedObserve() {
    var targetNode = document.querySelector("table");
    var config = {attributes: true, childList: true, subtree: true};

    var observer = new MutationObserver(clickListener);

    observer.observe(targetNode, config);
}