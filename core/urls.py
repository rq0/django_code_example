# -*- coding: utf-8 -*-

from django.conf.urls import url

from core.views import (LoginView, RentRequestEditView)

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name="core_login"),

    url(r'^rent/(?P<rent_id>\d+)/edit/$', RentRequestEditView.as_view(), name="rent_request_edit"),
]
