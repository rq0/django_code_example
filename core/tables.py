from django.utils.html import format_html
from django_tables2 import tables, A, LinkColumn

from core.models import RentRequest, EducationGroup


class RentRequestTable(tables.Table):
    edit = LinkColumn("rent_request_edit", text='edit', args=[A('pk')])

    def __init__(self, *args, **kwargs):
        self.base_columns['requester__student_set__all__0'].verbose_name = 'Обучающийся'
        self.base_columns['requester__teacher_set__all__0'].verbose_name = 'Преподаватель'
        self.base_columns['rentrequestequipment_set__all'].verbose_name = 'Оборудование'
        self.base_columns['edit'].verbose_name = ''
        super(RentRequestTable, self).__init__(*args, **kwargs)

    @staticmethod
    def render_edit():
        return format_html('<i class="edit outline icon" title="Редактировать запись"></i>')

    @staticmethod
    def render_rentrequestequipment_set__all(value):
        return ''.join("%s;" % request_equipment for request_equipment in value)

    class Meta:
        model = RentRequest
        fields = [
            'room',
            'start_date_time',
            'duration',
            'rentrequestequipment_set__all',
            'requester__student_set__all__0',
            'requester__teacher_set__all__0',
            'requester__phone',
        ]
        attrs = {"class": "ui selectable celled table"}


class ProgramReportTable(tables.Table):

    def __init__(self, *args, **kwargs):
        self.base_columns['module__all'].verbose_name = 'Количество часов'
        self.base_columns['module__all__0__skill__name'].verbose_name = 'Компетенция'
        super(ProgramReportTable, self).__init__(*args, **kwargs)

    @staticmethod
    def render_module__all__0__skill__name(value):
        # Не убирать это, иначе django_table2 падает на проверке существования компетенции
        return value

    @staticmethod
    def render_students(value):
        # Не убирать это, иначе django_table2 падает на проверке существования
        return len(value.all())

    @staticmethod
    def render_module__all(value):
        duration = 0
        for module in value:
            for theme in module.theme.all():
                duration += theme.duration
        return duration

    class Meta:
        model = EducationGroup
        fields = [
            'module__all__0__skill__name',
            'name',
            'program_type',
            'program_subtype',
            'module__all',
            'students',
        ]
        attrs = {"class": "ui selectable celled table"}
