from django.contrib.auth import get_user_model

from core.choices import UserChoices
from core.filters import UserReportFilter
from core.tables import UserReportTable
from core.views.templates import TableListView2


class UserReportView(TableListView2):
    meta_title = 'Пользователи'
    model = get_user_model()
    table_class = UserReportTable
    filterset_class = UserReportFilter
    perms = [UserChoices.ADMIN]

    def get_queryset(self):
        user_filter = {}
        queryset = super(UserReportView, self).get_queryset().prefetch_related(
            'student_set',).filter(
            **user_filter)
        self.filter = UserReportFilter(self.request.GET, queryset=queryset)
        return self.filter.qs
