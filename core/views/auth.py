from django.contrib.auth import login, logout, get_user_model
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView, RedirectView

from core.forms import CoreAuthenticationForm


class CoreLoginView(FormView):
    template_name = 'login.jinja2'
    form_class = CoreAuthenticationForm

    def auth(self, user):
        login(self.request, user)
        if self.request.user:
            self.request.session.update({
                'user': self.request.user.__json__()
            })
            if 'next=' in self.request.META.get('HTTP_REFERER', ''):
                next_url = self.request.META['HTTP_REFERER'][self.request.META['HTTP_REFERER'].index('next=') + 5:]
                return HttpResponseRedirect(next_url)
            return HttpResponseRedirect(reverse_lazy('rent_request_edit'))
        logout(self.request)
        return HttpResponseRedirect(reverse_lazy('core_login'))

    def form_valid(self, form):
        return self.auth(form.get_user())


class CoreLogoutView(RedirectView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse_lazy('core_login'))


class LoginRequiredMixin(object):
    def __init__(self):
        self.next_param = ''

    def logout_and_redirect(self, request):
        if request.path != '/':
            self.next_param = '?next=' + request.path
        return HttpResponseRedirect(reverse("core_login") + self.next_param)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            return self.logout_and_redirect(request)
        if not request.session.get('user'):
            return self.logout_and_redirect(request)
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)