import datetime
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.utils.functional import cached_property
from django.views.generic import ListView, UpdateView
from django_tables2 import SingleTableMixin
from django_tables2.export import ExportMixin

from core.choices import UserChoices
from core.models import OrganizationManager, Teacher, Student
from core.views.permission_mixins import PermissionMixin

YEAR = datetime.datetime.today().year


class TemplateDataMixin(object):
    site_name = 'projectname'
    meta_title = ''
    page_header = ''
    page_header_extra = ''
    page_help_text = ''
    page_help_url = ''
    success_url = reverse_lazy('core_login')
    return_url = reverse_lazy('core_login')

    def get_meta_title(self):
        return self.meta_title

    def get_page_header(self):
        return self.page_header or self.get_meta_title() or 'Нет названия'

    def get_page_header_extra(self):
        return self.page_header_extra

    def get_page_help_text(self):
        return self.page_help_text

    def get_page_help_url(self):
        return self.page_help_url

    def get_success_url(self):
        return self.success_url

    def get_return_url(self):
        return self.return_url

    def get_context_data(self, **kwargs):
        ctx = super(TemplateDataMixin, self).get_context_data(**kwargs)
        ctx.update(
            meta_title=self.get_meta_title(),
            page_header=self.get_page_header(),
            site_name=self.site_name,
            page_header_extra=self.get_page_header_extra(),
            page_help_text=self.get_page_help_text(),
            page_help_url=self.get_page_help_url(),
            success_url=self.get_success_url(),
            return_url=self.get_return_url(),
        )
        return ctx


class BaseMixin(PermissionMixin, TemplateDataMixin):

    def dispatch(self, request, *args, **kwargs):
        # Если права не объявлены, то дать доступ для всех
        if not hasattr(self, 'perms') or not len(self.perms):
            self.perms = [None, *UserChoices.AUTH_LIST]
        return super().dispatch(request, *args, **kwargs)

    def __init__(self):
        self.core_user = None
        self.request = None
        self.next_param = ''

    def logout_and_redirect(self, request):
        if request.path != '/':
            self.next_param = '?next=' + request.path
        return HttpResponseRedirect(reverse("core_login") + self.next_param)

    @cached_property
    def user_type(self):
        try:
            session_user = self.session_user
            if session_user:
                return session_user['user_type']
            else:
                return None
        except AttributeError:
            return None
        except KeyError:
            return None

    @cached_property
    def session_user(self):
        try:
            return self.request.session['user']
        except AttributeError:
            return None
        except KeyError:
            return None

    @cached_property
    def actor(self):
        actor_objects = None
        if self.user_type == UserChoices.STUDENT:
            actor_objects = Student.objects

        if self.user_type == UserChoices.TEACHER:
            actor_objects = Teacher.objects

        if self.user_type == UserChoices.ORGANIZATION_MANAGER:
            actor_objects = OrganizationManager.objects

        if actor_objects:
            return actor_objects.select_related('user').get(user_id=self.session_user['id'])
        else:
            return None

    def get_context_data(self, **kwargs):
        ctx = super(BaseMixin, self).get_context_data(**kwargs)
        ctx.update(
            year=YEAR,
            user=self.session_user,
            user_name=self.user_printable_name,
            user_type=self.user_type,
            UserChoices=UserChoices,
            actor=self.actor
        )
        return ctx


class CoreListView(ListView):
    filter = None

    @property
    def get_request_params(self):
        get_param = ''
        for key, v in self.request.GET.items():
            if key != 'page':
                value_list = self.request.GET.getlist(key)
                key_params = ['&%s=%s' % (key, val) for val in value_list]
                get_param = get_param.join(key_params)
        return get_param

    def get_context_data(self, **kwargs):
        ctx = super(CoreListView, self).get_context_data(**kwargs)
        ctx.update(
            filter_form=self.filter,
            GET_request_params=self.get_request_params
        )
        return ctx


class CardsListView(BaseMixin, CoreListView):
    add_url = ''
    add_card_title = ''
    add_card_action_title = ''
    paginate_by = 13

    def get_add_url(self):
        return self.add_url

    def get_add_card_action_title(self):
        return self.add_card_action_title

    def get_add_card_title(self):
        return self.add_card_title

    def get_context_data(self, **kwargs):
        ctx = super(CardsListView, self).get_context_data(**kwargs)
        ctx.update(
            add_url=self.get_add_url(),
            add_card_action_title=self.get_add_card_action_title(),
            add_card_title=self.get_add_card_title(),
        )
        return ctx


class CoreUpdateView(BaseMixin, UpdateView):
    def get_initial(self):
        initial = super(CoreUpdateView, self).get_initial().copy()
        initial.update({
            'view_type': "edit"
        })
        return initial


class TableListView2(ExportMixin, BaseMixin, SingleTableMixin, CoreListView):
    template_name = '_general_table.jinja2'
