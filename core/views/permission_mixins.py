from django.http import Http404


class PermissionMixin(object):
    user_type = None
    perms = []

    def dispatch(self, request, *args, **kwargs):
        if self.user_type in self.perms:
            return super().dispatch(request, *args, **kwargs)
        raise Http404
