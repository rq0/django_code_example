# -*- coding: utf-8 -*-
import datetime
import logging
from django.contrib.auth import get_user_model
from django.db.models import Prefetch
from django.db.transaction import atomic
from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView

from core.choices import UserChoices
from core.filters import (
    OneTimeEventFilter)
from core.forms import (
    StudentForm, EquipmentForm, RentRequestForm)
from core.models import (
    Student, OneTimeEvent, Equipment, OrganizationManager, RentRequest)
from core.views.templates import TableListView, CoreUpdateView, CardsListView, BaseMixin

logger = logging.getLogger('core')


class StudentRegisterView(BaseMixin, CreateView):
    template_name = '_general_edit.jinja2'
    form_class = StudentForm
    meta_title = 'Регистрация обучающегося'
    model = Student
    perms = [None, ]

    @atomic
    def form_valid(self, form):
        return super(StudentRegisterView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super(StudentRegisterView, self).get_context_data(**kwargs)
        return ctx

    def get_success_url(self):
        return "%s?%s" % (reverse_lazy('core_login'), 'student_successfully_registered')

    def get_return_url(self):
        return "%s?%s" % (reverse_lazy('core_login'), 'student_registration_failed')


class OneTimeEventListView(CardsListView):
    template_name = 'one_time_event_list.jinja2'
    meta_title = 'Мероприятия'
    add_url = reverse_lazy('one_time_event_new')
    add_card_title = 'Мероприятие'
    add_card_action_title = 'Создать мероприятие'
    model = OneTimeEvent

    def get_queryset(self):
        prefetch_list = []
        if self.session_user and self.session_user['id']:
            prefetch_list.append(
                Prefetch(
                    "visitor",
                    queryset=get_user_model().objects.filter(id=self.session_user['id']),
                    to_attr="visitor_filter_by_me"
                )
            )

        queryset = super(OneTimeEventListView, self).get_queryset().prefetch_related(
            *prefetch_list
        )

        if self.user_type != UserChoices.ADMIN:
            queryset = queryset.filter(event_status=self.model.STATUS_ACCEPT)
        self.filter = OneTimeEventFilter(self.request.GET, queryset=queryset)
        return self.filter.qs

    def get_context_data(self, **kwargs):
        ctx = super(OneTimeEventListView, self).get_context_data(**kwargs)
        ctx.update(
            datetime_now=datetime.datetime.now()
        )
        return ctx


class EquipmentDeleteView(BaseMixin, DeleteView):
    model = Equipment
    perms = [UserChoices.ADMIN]

    def get_success_url(self):
        return reverse_lazy('equipment_list')


class EquipmentNewView(BaseMixin, CreateView):
    template_name = '_general_edit.jinja2'
    form_class = EquipmentForm
    meta_title = 'Создание средства обучения'
    model = Equipment
    success_url = reverse_lazy('equipment_list')
    return_url = reverse_lazy('equipment_list')
    perms = [UserChoices.ADMIN, UserChoices.ORGANIZATION_MANAGER]

    def get_initial(self):
        initial = super(EquipmentNewView, self).get_initial().copy()
        is_manager = self.user_type == UserChoices.ORGANIZATION_MANAGER
        if is_manager:
            initial.update({
                "is_manager": is_manager,
                "organization_id": self.actor.organization_id
            })
        return initial


class EquipmentEditView(CoreUpdateView):
    template_name = '_general_edit.jinja2'
    form_class = EquipmentForm
    meta_title = 'Редактирование средства обучения'
    model = Equipment
    success_url = reverse_lazy('equipment_list')
    return_url = reverse_lazy('equipment_list')
    perms = [UserChoices.ADMIN, UserChoices.ORGANIZATION_MANAGER]

    def get_object(self, queryset=None):
        equipment_filter = {}
        if self.user_type == UserChoices.ORGANIZATION_MANAGER:
            equipment_filter['building__organization_id'] = self.actor.organization_id
        try:
            return self.model.objects.get(
                id=self.kwargs.get('equipment_id'),
                **equipment_filter
            )
        except self.model.DoesNotExist:
            raise Http404

    def get_initial(self):
        initial = super(EquipmentEditView, self).get_initial().copy()
        is_manager = self.user_type == UserChoices.ORGANIZATION_MANAGER
        if is_manager:
            initial.update({
                "is_manager": is_manager,
                "organization_id": self.actor.organization_id
            })
        return initial


class OrganizationManagerListView(TableListView):
    meta_title = 'Менеджеры организации'
    add_url = reverse_lazy('organization_manager_new')
    model = OrganizationManager
    verbose_flds = [
        'organization',
        'user',
        'last_name',
        'first_name',
        'middle_name',
        'gender',
        'birth_date',
    ]
    perms = [UserChoices.ADMIN]


class RentRequestNewView(CreateView, CardsListView):
    form_class = RentRequestForm
    meta_title = 'Бронирование оборудования'
    model = RentRequest
    return_url = reverse_lazy('core_login')
    perms = UserChoices.AUTH_LIST

    def get_template_names(self):
        if 'room' in self.request.GET:
            return 'rent_request_new.jinja2'
        return 'rent_room_list.jinja2'

    def get_success_url(self):
        return "%s?%s" % (reverse_lazy('profile'), 'rent_successfully_requested')

    def get_initial(self):
        initial = super(RentRequestNewView, self).get_initial().copy()
        if 'room' in self.request.GET:
            initial.update({
                'room_id': self.request.GET.get('room'),
            })
        return initial

    def form_valid(self, form):
        form.instance.requester_id = self.session_user['id']
        return super(RentRequestNewView, self).form_valid(form)


class RentRequestEditView(CoreUpdateView):
    template_name = '_general_edit.jinja2'
    form_class = RentRequestForm
    meta_title = 'Редактирование бронирования оборудования'
    model = RentRequest
    success_url = reverse_lazy('rent_request_list')
    return_url = reverse_lazy('rent_request_list')
    perms = [UserChoices.ADMIN]

    def get_object(self, queryset=None):
        try:
            return self.model.objects.get(
                id=self.kwargs.get('rent_id'))
        except self.model.DoesNotExist:
            raise Http404
