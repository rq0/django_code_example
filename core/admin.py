from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from core.models import (
    User, Student, SkillGroup, Skill, Theme, Module, Program, EducationRequest, EducationGroup, Teacher,
    CompanyEducationRequest, OneTimeEvent, Building, Equipment, Link, LinkCategory, News, NewsImage, Room)

admin.site.register(User, UserAdmin)

model_list = [
    Student,
    Teacher,
    SkillGroup,
    Skill,
    Theme,
    Module,
    Program,
    EducationRequest,
    EducationGroup,
    CompanyEducationRequest,
    OneTimeEvent,
    News,
    NewsImage,
    Building,
    Equipment,
    Link,
    LinkCategory,
    Room,
]

for model in model_list:
    admin.site.register(model)
