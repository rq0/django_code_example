from django.contrib.auth import get_user_model
from django_filters import FilterSet, MultipleChoiceFilter


class CoreFilterSet(FilterSet):
    def __init__(self, *args, **kwargs):
        super(CoreFilterSet, self).__init__(*args, **kwargs)

        if 'deleted_at' in self.filters:
            self.filters.pop('deleted_at')

        from django.forms.widgets import Select, Textarea, DateInput, DateTimeInput, TimeInput
        for field_name, field in self.filters.items():
            form_field = field.field
            form_field.widget.attrs.update(autocomplete="off", clearable='true')
            cl = form_field.widget.attrs.get(u'class', u'')
            if isinstance(form_field.widget, Select):
                cl = (cl + u'ui clearable search selection dropdown').strip()
            if isinstance(form_field.widget, Textarea):
                form_field.widget.attrs.update(rows='2')
            if isinstance(form_field.widget, DateInput):
                form_field.widget.attrs.update(datepicker='true')
            if isinstance(form_field.widget, TimeInput):
                form_field.widget.attrs.update(timepicker='true')
            if isinstance(form_field.widget, DateTimeInput):
                form_field.widget.attrs.update(datepicker='true')
                form_field.widget.attrs.update(timepicker='true')

            form_field.widget.attrs[u'class'] = cl


    class Meta:
        abstract = True


class UserReportFilter(CoreFilterSet):

    def __init__(self, *args, **kwargs):
        self.base_filters['student__educationgroup'].label = 'Учебные группы'
        self.base_filters['student__educationgroup__program'].label = 'Учебные группы по программе'
        self.base_filters['student__educationgroup__date_start__lt'].label = 'Учебные группы дата начала меньше'
        self.base_filters['student__educationgroup__date_start__gt'].label = 'Учебные группы дата начала больше'
        self.base_filters['student__educationgroup__date_end__lt'].label = 'Учебные группы дата окончания меньше'
        self.base_filters['student__educationgroup__date_end__gt'].label = 'Учебные группы дата окончания больше'
        self.base_filters['student__birth_date__year'].label = 'Год рождения обучающегося'
        self.base_filters['onetimeevent'].label = 'Мероприятия'
        self.base_filters['onetimeevent__start_date_time__lt'].label = 'Мероприятия дата начала меньше'
        self.base_filters['onetimeevent__start_date_time__gt'].label = 'Мероприятия дата начала больше'
        super(UserReportFilter, self).__init__(*args, **kwargs)

    class Meta:
        model = get_user_model()
        fields = {
            'user_type': ['exact'],
            'student__birth_date': ['year__exact'],
            'student__educationgroup': ['exact'],
            'student__educationgroup__program': ['exact'],
            'student__educationgroup__date_start': ['gt', 'lt'],
            'student__educationgroup__date_end': ['gt', 'lt'],
            'onetimeevent': ['exact'],
            'onetimeevent__start_date_time': ['gt', 'lt'],
            'phone': ['icontains'],
            'username': ['icontains'],
            'email': ['icontains'],
            'date_joined': ['lte', 'gte'],
        }
