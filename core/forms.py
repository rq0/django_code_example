# -*- coding: utf-8 -*-
import datetime
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ValidationError

from core.choices import UserChoices
from core.models import (
    Teacher, Equipment, RentRequest, RentRequestEquipment)


class CoreAuthenticationForm(AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs):
        super(CoreAuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'])
            elif not self.user_cache.is_active:
                raise forms.ValidationError(self.error_messages['inactive'])
        return self.cleaned_data


class CoreForm(forms.ModelForm):
    divided_fields = []
    create_only_fields = []
    edit_only_fields = []

    @staticmethod
    def validate_date(value):
        u"""Во всех datefield валидируем, что дата не может быть меньше 1901 года"""
        max_year = datetime.datetime.now().year + 10
        if value.year < 1901:
            raise ValidationError("Нельзя указывать год меньше 1901!")
        elif value.year > max_year:
            raise ValidationError("Нельзя указывать год больше %s!" % max_year)

    def exclude_create_only_fields(self):
        if 'view_type' in self.initial and self.initial['view_type'] == "edit":
            for create_only_field in self.create_only_fields:
                self.fields.pop(create_only_field)

    def exclude_edit_only_fields(self):
        if 'view_type' not in self.initial or self.initial['view_type'] != "edit":
            for edit_only_field in self.edit_only_fields:
                self.fields.pop(edit_only_field)

    def exclude_deleted_at_field(self):
        if 'deleted_at' in self.fields:
            self.fields.pop('deleted_at')

    def add_widgets(self):
        from django.forms.widgets import Select, Textarea, DateInput, DateTimeInput, TimeInput
        for field_name, field in self.fields.items():
            field.widget.attrs.update(autocomplete="off", clearable='true')
            cl = field.widget.attrs.get('class', '')
            if isinstance(field.widget, Select):
                cl = (cl + 'ui clearable search selection dropdown').strip()
            if isinstance(field.widget, Textarea):
                field.widget.attrs.update(rows='2')
            if isinstance(field.widget, DateInput):
                field.widget.format = '%Y-%m-%d'
                field.widget.attrs.update(datepicker='true')
                field.validators.append(self.validate_date)
            if isinstance(field.widget, DateTimeInput):
                field.widget.format = '%Y-%m-%d %H:%M:%S'
                field.widget.attrs.update(datepicker='true')
                field.widget.attrs.update(timepicker='true')
                field.validators.append(self.validate_date)
            if isinstance(field.widget, TimeInput):
                field.widget.attrs.update(timepicker='true')
            field.widget.attrs['class'] = cl

    def add_divider(self):
        for field_param in self.divided_fields:
            if field_param['field_name'] in self.fields:
                field = self.fields[field_param['field_name']]
                field.widget.attrs.update(divider=field_param['field_group_name'])

    def __init__(self, *args, **kwargs):
        super(CoreForm, self).__init__(*args, **kwargs)
        self.exclude_deleted_at_field()
        self.exclude_create_only_fields()
        self.exclude_edit_only_fields()

        self.add_widgets()
        self.add_divider()


class ActorForm(CoreForm):
    username = forms.RegexField(
        label="Логин",
        max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text="Допустимо использовать буквы, цифры или знаки @/./+/-/_ только.",
        error_messages={
            'invalid': "Вы использовали недопустимые символы. Допустимы буквы, цифры или знаки @/./+/-/_."})
    password = forms.CharField(label="Пароль", widget=forms.PasswordInput)
    password_confirm = forms.CharField(label="Повторите пароль", widget=forms.PasswordInput)
    phone = forms.CharField(label='Сотовый телефон', max_length=30, help_text="Формат: \"+7(999)999-99-99\"")
    email = forms.CharField(label='Электронная почта', required=True)
    privacy_confirm = forms.BooleanField(
        label='Я подтверждаю свое согласие на сбор, обработку и хранение персональных данных',
        required=True)

    error_messages = {
        'duplicate_username': "Пользователь с таким логином уже существует.",
        'password_mismatch': "Введенные пароли не совпадают.",
    }

    create_only_fields = [
        'password',
        'password_confirm',
        'privacy_confirm',
    ]

    def clean_username(self):
        username = self.cleaned_data["username"]

        if not get_user_model().objects.filter(username=username).exists():
            return username

        if 'view_type' in self.initial and self.initial['view_type'] == "edit":
            return username

        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_password_confirm(self):
        password = self.cleaned_data.get("password")
        password_confirm = self.cleaned_data["password_confirm"]
        if password != password_confirm:
            raise forms.ValidationError(self.error_messages['password_mismatch'])
        return password

    def save(self, commit=True):
        username = self.cleaned_data["username"]
        if 'view_type' in self.initial and self.initial['view_type'] == "edit":
            same_username = self.instance.user.username == username
            if same_username:
                user = self.instance.user
            elif not len(get_user_model().objects.filter(username=username)):
                user = self.instance.user
                user.username = username
            else:
                raise forms.ValidationError(self.error_messages['duplicate_username'])
        else:
            user = get_user_model().objects.create_user(
                username=username,
                password=self.cleaned_data['password']
            )
        user.phone = self.cleaned_data['phone']
        user.email = self.cleaned_data['email']
        user.user_type = UserChoices.STUDENT
        user.save()

        self.instance.user = user
        return self.instance.save()


class TeacherForm(ActorForm):
    field_order = [
        'username', 'password', 'password_confirm',
        'last_name', 'first_name', 'middle_name',
        'gender', 'birth_date',
        'position', 'organization', 'skill',
        'email', 'phone',
    ]

    divided_fields = [{
        'field_name': 'username',
        'field_group_name': "Авторизационные данные"}, {
        'field_name': 'last_name',
        'field_group_name': "Персональные данные"}, {
        'field_name': 'position',
        'field_group_name': "Данные преподавателя"}, {
        'field_name': 'email',
        'field_group_name': "Контактная информация"}
    ]

    def __init__(self, *args, **kwargs):
        super(TeacherForm, self).__init__(*args, **kwargs)
        if 'is_manager' in self.initial and self.initial['is_manager']:
            self.fields.pop('organization')

    def save(self, commit=True):
        username = self.cleaned_data["username"]
        if 'view_type' in self.initial and self.initial['view_type'] == "edit":
            same_username = self.instance.user.username == username
            if same_username:
                user = self.instance.user
            elif not len(get_user_model().objects.filter(username=username)):
                user = self.instance.user
                user.username = username
            else:
                raise forms.ValidationError(self.error_messages['duplicate_username'])
        else:
            user = get_user_model().objects.create_user(
                username=username,
                password=self.cleaned_data['password']
            )
        user.phone = self.cleaned_data['phone']
        user.email = self.cleaned_data['email']
        user.user_type = UserChoices.TEACHER
        user.save()

        self.instance.user = user
        return self.instance.save()

    class Meta:
        model = Teacher
        exclude = ['photo', 'user']


class RentRequestForm(CoreForm):
    equipment = forms.ModelChoiceField(
        queryset=Equipment.objects.filter(rent_available=True), label='Средство обучения', required=False)
    amount = forms.IntegerField(label='Количество', required=False, min_value=1)

    divided_fields = [{
        'field_name': 'equipment',
        'field_group_name': "Требуемое оборудование"
    }]

    field_order = [
        'start_date_time',
        'room',
        'duration',
        'equipment',
        'amount',
    ]

    edit_only_fields = [
        'status',
    ]

    create_only_fields = [
        'equipment',
        'amount',
    ]

    def __init__(self, *args, **kwargs):
        super(RentRequestForm, self).__init__(*args, **kwargs)
        self.fields['room'].queryset = self.fields['room'].queryset.filter(
            rent_available=True
        )
        if 'room_id' in self.initial:
            self.fields['room'].queryset = self.fields['room'].queryset.filter(
                id=self.initial['room_id']
            )
            self.fields['room'].initial = self.initial['room_id']
            self.fields['equipment'].queryset = self.fields['equipment'].queryset.filter(
                room_id=self.initial['room_id'],
                rent_available=True
            )
            self.fields['room'].widget = forms.HiddenInput()

    def save(self, commit=True):
        rent_request = super(RentRequestForm, self).save(commit=True)

        rent_request_equipment_list = []
        for field_name in self.data:
            if 'equipment' in field_name and int(self.data[field_name]):
                rent_request_equipment_list.append(RentRequestEquipment(
                    rent_request_id=rent_request.id,
                    equipment_id=field_name.split('equipment_')[1],
                    amount=self.data[field_name]
                ))

        RentRequestEquipment.objects.bulk_create(rent_request_equipment_list)
        return rent_request

    class Meta:
        model = RentRequest
        exclude = ['requester']
