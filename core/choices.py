# -*- coding: utf-8 -*-

BOOLEAN_CHOICES = [
    ('', '-' * 8),
    (1, 'Да'),
    (0, 'Нет'),
]


class RequestStatusChoices(object):
    STATUS_DEFAULT = 0
    STATUS_ACCEPT = 1
    STATUS_DECLINE = 2
    STATUS_OUTDATED = 3

    STATUS_CHOICES = [
        (STATUS_DEFAULT, 'На рассмотрении'),
        (STATUS_ACCEPT, 'Одобрена'),
        (STATUS_DECLINE, 'Отклонена'),
        (STATUS_OUTDATED, 'Устарела'),
    ]


class HumanChoices(object):
    FEMALE = 0
    MALE = 1

    GENDER_CHOICES = [
        (FEMALE, 'женский'),
        (MALE, 'мужской')
    ]


class OneTimeEventChoices(RequestStatusChoices, object):
    FORMAT_LECTURE = 0
    FORMAT_PRACTICE = 1
    FORMAT_SEMINAR = 2
    FORMAT_EXCURSION = 3
    FORMAT_OPEN_DAY = 4
    FORMAT_QUEST = 5
    FORMAT_MASTER_CLASS = 6
    FORMAT_PRESENTATION = 7
    FORMAT_OPEN_DIALOG = 8
    FORMAT_CREATIVE_ACTIVITY = 9
    FORMAT_TRAINING = 10
    FORMAT_FILM_LECTURE = 11

    FORMAT_CHOICES = [
        (FORMAT_LECTURE, 'Лекция'),
        (FORMAT_SEMINAR, 'Семинар'),
        (FORMAT_PRACTICE, 'Практикум'),
        (FORMAT_EXCURSION, 'Экскурсия'),
        (FORMAT_OPEN_DAY, 'День открытых дверей'),
        (FORMAT_QUEST, 'Квест'),
        (FORMAT_MASTER_CLASS, 'Мастер - класс'),
        (FORMAT_PRESENTATION, 'Презентационная сессия'),
        (FORMAT_OPEN_DIALOG, 'Открытый диалог'),
        (FORMAT_CREATIVE_ACTIVITY, 'Творческое занятие'),
        (FORMAT_TRAINING, 'Тренинг'),
        (FORMAT_FILM_LECTURE, 'Кинолекторий'),
    ]
