from django.core.validators import MinValueValidator
from django.db import models

from core.choices import RequestStatusChoices
from .base import CoreBaseModel, APP_LABEL

__all__ = (
    "Equipment",
    "RentRequest",
    "RentRequestEquipment",
)


class Equipment(CoreBaseModel):
    """Средство обучения"""
    name = models.CharField(verbose_name='Наименование', max_length=128)
    room = models.ForeignKey(Room, verbose_name='Площадка', null=True, on_delete=models.SET_NULL)
    inventory_number = models.CharField(verbose_name='Инвентарный номер', max_length=64, blank=True, default='')
    amount = models.PositiveIntegerField(verbose_name='Количество', default=1, validators=[
        MinValueValidator(0)
    ])
    rent_available = models.BooleanField('Доступно для бронирования', default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
        app_label = APP_LABEL
        verbose_name = "Средство обучения"
        verbose_name_plural = "Средства обучения"


class RentRequest(RequestStatusChoices, CoreBaseModel):
    room = models.ForeignKey(Room, verbose_name='Площадка', null=True, on_delete=models.SET_NULL)
    start_date_time = models.DateTimeField(verbose_name='Дата бронирования', null=True)
    duration = models.FloatField(verbose_name=u'Продолжительность (часов)', default=1.0)
    requester = models.ForeignKey(
        'User', blank=True, null=True, on_delete=models.SET_NULL, related_name='rent_requester')
    status = models.PositiveSmallIntegerField(
        choices=RequestStatusChoices.STATUS_CHOICES, default=RequestStatusChoices.STATUS_DEFAULT,
        verbose_name='Статус запроса')

    def __str__(self):
        return "%s %s %s" % (self.room, self.start_date_time, self.requester)

    class Meta:
        ordering = ['-start_date_time']
        app_label = APP_LABEL
        verbose_name = u"Бронирование оборудования и аудиторию"
        verbose_name_plural = u"Бронирования оборудования и аудитории"


class RentRequestEquipment(CoreBaseModel):
    rent_request = models.ForeignKey(
        RentRequest, verbose_name='Бронь оборудования и аудитории', on_delete=models.DO_NOTHING)
    equipment = models.ForeignKey(Equipment, verbose_name='Средство обучения', blank=True, on_delete=models.DO_NOTHING)
    amount = models.PositiveIntegerField(verbose_name='Количество', default=1, validators=[MinValueValidator(0)])

    def __str__(self):
        return "%s: %sшт." % (self.equipment, self.amount)

    class Meta:
        ordering = ['-id']
        app_label = APP_LABEL
        verbose_name = u"Бронирование оборудования и аудиторию"
        verbose_name_plural = u"Бронирования оборудования и аудитории"

