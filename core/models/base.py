# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib.auth.models import AbstractUser
from django.db import models
from imagekit.models import ProcessedImageField

from core.choices import UserChoices, StudentChoices, HumanChoices

__all__ = (
    "APP_LABEL",
    "CoreBaseModel",
    "Human",
    "User",
    "Student",
)

APP_LABEL = 'core'


class SoftDeletionQuerySet(models.QuerySet):
    def order_by(self, *field_names):
        return super(SoftDeletionQuerySet, self).order_by(*field_names)

    def delete(self):
        return super(SoftDeletionQuerySet, self).update(deleted_at=datetime.now)

    def hard_delete(self):
        return super(SoftDeletionQuerySet, self).delete()

    def alive(self):
        return self.filter(deleted_at=None)

    def dead(self):
        return self.exclude(deleted_at=None)


class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super(SoftDeletionManager, self).__init__(*args, *kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class SoftDeletionModel(models.Model):
    deleted_at = models.DateTimeField(blank=True, null=True, verbose_name='Удалено')

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = datetime.now()
        self.save()

    def hard_delete(self):
        super(SoftDeletionModel, self).delete()


class CoreBaseModel(SoftDeletionModel):
    class Meta:
        ordering = ['-id']
        abstract = True
        app_label = APP_LABEL


class User(UserChoices, AbstractUser):
    phone = models.CharField(
        u'Сотовый телефон', max_length=30, default='', blank=True, help_text=u"Формат: \"+7(999)999-99-99\"")
    user_type = models.PositiveSmallIntegerField(
        choices=UserChoices.USER_TYPE_CHOICES, null=True, blank=True,
        verbose_name=u'Тип пользователя')

    def __json__(self):
        return dict(
            id=self.id,
            user_type=self.user_type,
            phone=self.phone,
            email=self.email,
            is_active=self.is_active
        )

    class Meta:
        db_table = 'auth_user'
        ordering = ('id', 'username', 'first_name', 'last_name', 'password')
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Human(HumanChoices, CoreBaseModel):
    u"""Базовые поля людей"""
    # Общие сведения
    last_name = models.CharField(u'Фамилия', max_length=64)
    first_name = models.CharField(u'Имя', max_length=64)
    middle_name = models.CharField(u'Отчество', max_length=64, blank=True)

    photo = ProcessedImageField(
        upload_to='avatars', verbose_name=u'Фото', null=True, blank=True,
        format='JPEG', options={'quality': 40})
    gender = models.IntegerField(u'Пол', choices=HumanChoices.GENDER_CHOICES)
    birth_date = models.DateField(u'Дата рождения', blank=True, null=True)

    user = models.ForeignKey(User, verbose_name=u'Пользователь', null=True, on_delete=models.DO_NOTHING, )

    @property
    def last_first_middle_name(self):
        return "%s %s %s" % (self.last_name, self.first_name, self.middle_name)

    class Meta:
        abstract = True
        app_label = APP_LABEL


class Student(StudentChoices, Human):
    u"""Обучающийся"""
    student_type = models.PositiveSmallIntegerField(
        choices=StudentChoices.TARGET_CHOICES, verbose_name=u'Категория пользователя')

    def __str__(self):
        return u"%s %s" % (self.last_name, self.first_name)

    class Meta:
        app_label = APP_LABEL
        verbose_name = u"Обучающийся"
        verbose_name_plural = u"Обучающиеся"
        ordering = ('last_name', 'first_name', 'middle_name')
