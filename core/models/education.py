import uuid
from django.contrib.auth import get_user_model
from django.core.validators import EmailValidator
from django.db import models

from core.choices import (
    EducationRequestChoices)
from .base import CoreBaseModel, APP_LABEL

__all__ = (
    "EducationRequest",
)


class EducationRequest(EducationRequestChoices, CoreBaseModel):
    """Запрос на обучение"""
    module = models.ManyToManyField(Module, verbose_name='Модули')
    contact_name = models.CharField(max_length=128, null=True, verbose_name='Контактное имя')
    contact_email = models.CharField(
        max_length=128, null=True, verbose_name='Контактный email', validators=[EmailValidator])
    contact_phone = models.CharField(max_length=128, null=True, verbose_name='Контактный телефон')
    readable_key = models.UUIDField(
        primary_key=False, null=True, verbose_name='Ключ запроса на обучения', default=uuid.uuid4,
        max_length=8)
    status = models.PositiveSmallIntegerField(
        choices=EducationRequestChoices.STATUS_CHOICES, default=EducationRequestChoices.STATUS_DEFAULT,
        verbose_name='Статус запроса')
    requester = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.SET_NULL)
    create_date_time = models.DateTimeField(verbose_name='Дата заявки', auto_now_add=True)

    def __str__(self):
        return "Запрос на обучение №%s" % self.readable_key

    class Meta:
        ordering = ['-id']
        verbose_name = "Запрос на обучение из конструктора"
        verbose_name_plural = "Запросы на обучение из конструктора"
        app_label = APP_LABEL
