""" Default urlconf for config """

from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import logout
from django.views.static import serve

admin.autodiscover()

urlpatterns = [
    url(r'^', include('core.urls')),
    url(r'^admin/', admin.site.urls),
    # all-auth
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/logout/$', logout, {'next_page': '/'}),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += [url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT})]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls)), ]


def bad(request):
    """ Simulates a server error """
    1 / 0


urlpatterns += [url(r'^bad/$', bad), ]
