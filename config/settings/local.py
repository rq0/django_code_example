"""These settings overrides what's in settings/base.py"""

from . import base


INSTALLED_APPS = base.INSTALLED_APPS + [
    'django_nose',
    'debug_toolbar',
    # 'silk',
]

MIDDLEWARE = base.MIDDLEWARE + [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # 'silk.middleware.SilkyMiddleware',
]

local_machine_db = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'projectname',
        'USER': 'root',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS': {
            'charset': 'utf8',
            'use_unicode': True,
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
            'compress': True,
        },
    },
    'db_query_dirty_transactions': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'projectname',
        'USER': 'root',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS': {
            'charset': 'utf8',
            'use_unicode': True,
            "init_command":
                "SET default_storage_engine=INNODB; SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; "
                "SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'",
            'compress': True,
        },
    }
}


DATABASES = local_machine_db

# Recipients of traceback emails and other notifications.
ADMINS = [
    [u'admin', 'admin@projectname.ru'],
]

MANAGERS = ADMINS

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
# Debugging displays nice error messages, but leaks memory. Set this to False
# on all server instances and True only for development.
DEBUG = True

# Is this a development instance? Set this to True on development/master
# instances and False on stage/prod.
DEV = True


# Sessions
#
# By default, be at least somewhat secure with our session cookies.
SESSION_COOKIE_HTTPONLY = False

# Set this to true if you are using https
SESSION_COOKIE_SECURE = False

SECURE_HSTS_SECONDS = 0

SECURE_CONTENT_TYPE_NOSNIFF = False

SECURE_BROWSER_XSS_FILTER = False

SECURE_SSL_REDIRECT = False

CSRF_COOKIE_SECURE = False

X_FRAME_OPTIONS = "ALLOW"

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['192.168.1.7', 'localhost', '0.0.0.0', 'projectname.local']

# Uncomment these to activate and customize Celery:
# CELERY_ALWAYS_EAGER = False  # required to activate celeryd
# BROKER_HOST = 'localhost'
# BROKER_PORT = 5672
# BROKER_USER = 'django'
# BROKER_PASSWORD = 'django'
# BROKER_VHOST = 'django'
# CELERY_RESULT_BACKEND = 'amqp'

INTERNAL_IPS = ['127.0.0.1', '192.168.1.7', 'localhost', '0.0.0.0', 'projectname.local']

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]


def custom_show_toolbar(request):
    """ Only show the debug toolbar to users with the superuser flag. """
    return DEBUG


DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TOOLBAR_CALLBACK': 'config.settings.local.custom_show_toolbar',
    'HIDE_DJANGO_SQL': True,
    'TAG': 'body',
    'SHOW_TEMPLATE_CONTEXT': True,
    'ENABLE_STACKTRACES': True,
}