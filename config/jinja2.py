import datetime

from django.contrib.staticfiles.storage import staticfiles_storage
from django.db.models.fields.related import ManyToManyField
from django.urls import reverse
from jinja2 import Environment


def prop_in_obj(field_name, instance):
    """Ищем атрибут в объекте"""
    field_value = instance
    for f_name in field_name.split('.'):
        if hasattr(field_value, f_name):
            field_value = getattr(field_value, f_name)
            if callable(field_value):
                field_value = field_value()
        elif type(field_value) is dict:
            if field_name in field_value:
                field_value = field_value.get(field_name)
                if callable(field_value):
                    field_value = field_value()
            else:
                field_value = None
                break
        else:
            field_value = None
            break
    return field_value


def string_of(instance, field_name):
    """Строковое значения поля"""
    if '.' not in field_name and isinstance(instance._meta.get_field(field_name), ManyToManyField):
        field_value = '; '.join([str(related_object) for related_object in getattr(instance, field_name).all()])
    else:
        field_value = prop_in_obj(field_name, instance)
        if type(field_value) == str:
            if not len(field_value):
                return 'False'
            return (field_value[:140] + ' ...') if len(field_value) > 140 else field_value
        elif isinstance(field_value, bool):
            return '%s' % field_value
        elif isinstance(field_value, datetime.datetime) or isinstance(field_value, datetime.date):
            date_format = isinstance(field_value, datetime.date) and '%d.%m.%Y' or '%d.%m.%Y %H:%M:%S'
            try:
                return field_value.strftime(date_format)
            except ValueError:
                return "Неверный формат даты"
        elif type(field_value) == list:
            return ', '.join(map(str, field_value))
        elif field_value is None or field_value == '':
            return 'False'
        elif type(field_value) == int:
            if hasattr(instance, '_meta') and field_name in [i.name for i in instance._meta.fields]:
                field = instance._meta.get_field(field_name)
                if field.choices:
                    return str(getattr(instance, 'get_%s_display' % field_name)())
            return str(field_value)
    return field_value


def environment(**options):
    """
    Creates jinja2 environment during django initialization.
    """
    env = Environment(**options)

    # Globals
    env.globals.update(
        static=staticfiles_storage.url,
        url=reverse,
    )
    # Filters
    env.filters['string_of'] = string_of
    return env
