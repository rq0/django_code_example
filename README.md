
# Part of my Django2.2+ application #

## About ##


This project is about my skills in Django.

Django 2 (python3)
includes fomantic-uf, sweetalert2, django-tables2, django-filter, Allauth, and some other useful apps.


```
src                   # project root  
├── manage.py
├── /static
├── /config           # project configuration 
│   ├── /settings   
│   │   ├── base.py   # project settings   
│   │   ├── local.py  # local settings (development)
│   ├── urls.py        
│   ├── wsgi.py
├── /appname           # application example  
│   ├── models
│   ├── views
│   ├── forms.py
│   ├── filters.py    # django-filter
│   ├── tables.py     # django-tables2
│   ├── choices.py    # choices for ChoiceField
│   ├── urls.py
│   ├── jinja2        # app templates
│   ├── migrarions
    ... 
```

## MOST OF PROJECT CODE HAS BEEN REMOVED! THIS REPOSITORY IS ONLY EXAMPLE. ##


## Features ##

This project includes:

A set of basic templates and Twitter Bootstrap 3.3.5 (located in the
base app, with css and javascript included).

Templating:

- django templates for render errors like config/templates/404.html
- jinja2 templates for render app templates

Authentication, registration
- allauth 

Security:

- bleach
- bcrypt - uses bcrypt for password hashing by default

Caching:

- python-memcached

Admin:

- Includes django-debug-toolbar for development and production (enabled for superusers)

Testing:

- nose and django-nose
- pylint, pep8, and coverage

